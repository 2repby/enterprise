<div class="container">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование</th>
            <th scope="col">Материал</th>
            <th scope="col">Вес</th>
            <?php if(isset($_SESSION['username'])) echo "<th scope='col'>Действие</th>" ?>
        </tr>
        </thead>
        <tbody>
<?php
$query = $pdo->query('SELECT * FROM parts');
while ($row = $query->fetch())
{
?>

    <tr>
            <th scope='row'><?php echo $row['Part_ID'] ?></th>
            <td><?php echo $row['Part_name'] ?></td>
            <td><?php echo $row['Material'] ?> </td>
            <td><?php echo  $row['Weight'] ?> </td>
            <?php if(isset($_SESSION['username'])) echo "<td><a type='button' class='btn btn-danger' href='?C=1&A=2&part_id=".$row['Part_ID']."'> Удалить</a></td>" ?>
        </tr>

<?php } ?>
        </tbody>
    </table>





</div>
