<form method="get">
    <div class="form-group">
        <label for="partname">Наименование детали</label>
        <input type="text" class="form-control" id="partname" name="partname">
    </div>
    <div class="form-group">
        <label for="material">Материал</label>
        <input type="text" class="form-control" id="material" name="material">
    </div>
    <div class="form-group">
        <label for="weight">Вес</label>
        <input type="text" class="form-control" id="weight" name="weight" >
    </div>
    <input type="hidden" name="C" value="1" >
    <input type="hidden" name="A" value="1" >
    <button type="submit" class="btn btn-primary">Добавить</button>
</form>