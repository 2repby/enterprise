<?php
/**
 * Created by PhpStorm.
 * User: kda
 * Date: 06.03.2020
 * Time: 23:28
 */
$host = '127.0.0.1';
$db   = 'enterprise';
$user = 'kda';
$pass = 'pass';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);