-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 09 2020 г., 09:45
-- Версия сервера: 5.7.23
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `enterprise`
--

-- --------------------------------------------------------

--
-- Структура таблицы `parts`
--

CREATE TABLE `parts` (
  `Part_ID` int(10) UNSIGNED NOT NULL,
  `Part_name` varchar(80) DEFAULT NULL,
  `Material` varchar(80) DEFAULT NULL,
  `Weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `parts`
--

INSERT INTO `parts` (`Part_ID`, `Part_name`, `Material`, `Weight`) VALUES
(1, 'Поддон', 'Сталь', 32),
(2, 'Кресло', 'Дерево', 41),
(3, 'Штурвал', 'Резина', 63),
(4, 'Кнопка', 'Стекло', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `Project_ID` int(10) UNSIGNED NOT NULL,
  `Project_name` varchar(20) DEFAULT NULL,
  `Town_ID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`Project_ID`, `Project_name`, `Town_ID`) VALUES
(1, 'Автомобиль', 1),
(2, 'Компьютер', 2),
(3, 'Самолет', 2),
(4, 'Вертолет', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `providers`
--

CREATE TABLE `providers` (
  `Provider_ID` int(11) UNSIGNED NOT NULL,
  `Provider_name` varchar(20) DEFAULT NULL,
  `Town_ID` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `providers`
--

INSERT INTO `providers` (`Provider_ID`, `Provider_name`, `Town_ID`) VALUES
(1, 'Сигма', 2),
(2, 'Альфа', 3),
(3, 'Бета', 3),
(4, 'Гамма', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `supply`
--

CREATE TABLE `supply` (
  `Supply_ID` int(11) UNSIGNED NOT NULL,
  `Part_ID` int(11) UNSIGNED DEFAULT NULL,
  `Provider_ID` int(11) UNSIGNED DEFAULT NULL,
  `Project_ID` int(11) UNSIGNED DEFAULT NULL,
  `Quantity` int(11) UNSIGNED DEFAULT NULL,
  `Price` int(11) UNSIGNED DEFAULT NULL,
  `Begin_date` datetime DEFAULT NULL,
  `End_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `supply`
--

INSERT INTO `supply` (`Supply_ID`, `Part_ID`, `Provider_ID`, `Project_ID`, `Quantity`, `Price`, `Begin_date`, `End_date`) VALUES
(2, 1, 3, 2, 341, 260, '2008-09-05 20:02:37', '2012-11-16 20:02:43'),
(3, 3, 3, 1, 12, 190, '2011-11-29 20:02:46', '2013-01-19 20:02:48'),
(4, 3, 1, 1, 33, 300, '2008-09-19 20:02:53', '2011-11-13 20:03:01'),
(5, 4, 2, 3, 34, 455, '2011-11-08 10:00:16', '2011-11-11 10:00:20'),
(6, 2, 1, 3, 11, 45, '2016-03-08 09:29:41', '2016-03-01 09:29:43'),
(8, 1, 1, 1, 45, 345, '2020-03-07 02:02:00', '2020-03-21 02:02:00'),
(9, 1, 1, 1, 45, 345, '2020-03-07 02:02:00', '2020-03-21 02:02:00'),
(10, 1, 1, 1, 324, 235, '2020-03-20 01:01:00', '2020-03-14 01:01:00'),
(11, 1, 1, 1, 11, 22, '2020-03-08 01:01:00', '2020-03-07 01:01:00'),
(12, 3, 2, 3, 324, 11, '2020-03-10 11:01:00', '2020-03-12 01:01:00'),
(13, 3, 2, 3, 324, 11, '2020-03-10 11:01:00', '2020-03-12 01:01:00');

-- --------------------------------------------------------

--
-- Структура таблицы `towns`
--

CREATE TABLE `towns` (
  `Town_ID` int(11) UNSIGNED NOT NULL,
  `Town_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `towns`
--

INSERT INTO `towns` (`Town_ID`, `Town_name`) VALUES
(1, 'Сургут'),
(2, 'Москва'),
(3, 'Томск'),
(4, 'Новосибирск');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `md5password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `md5password`) VALUES
(1, 'kda', '1a1dc91c907325c69271ddf0c944bc72');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `parts`
--
ALTER TABLE `parts`
  ADD PRIMARY KEY (`Part_ID`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`Project_ID`),
  ADD KEY `Town_ID` (`Town_ID`);

--
-- Индексы таблицы `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`Provider_ID`),
  ADD KEY `Town_ID` (`Town_ID`);

--
-- Индексы таблицы `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`Supply_ID`),
  ADD KEY `Part_ID` (`Part_ID`),
  ADD KEY `Provider_ID` (`Provider_ID`),
  ADD KEY `Project_ID` (`Project_ID`);

--
-- Индексы таблицы `towns`
--
ALTER TABLE `towns`
  ADD PRIMARY KEY (`Town_ID`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `parts`
--
ALTER TABLE `parts`
  MODIFY `Part_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `Project_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `providers`
--
ALTER TABLE `providers`
  MODIFY `Provider_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `supply`
--
ALTER TABLE `supply`
  MODIFY `Supply_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `towns`
--
ALTER TABLE `towns`
  MODIFY `Town_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`Town_ID`) REFERENCES `towns` (`Town_ID`);

--
-- Ограничения внешнего ключа таблицы `providers`
--
ALTER TABLE `providers`
  ADD CONSTRAINT `providers_ibfk_1` FOREIGN KEY (`Town_ID`) REFERENCES `towns` (`Town_ID`);

--
-- Ограничения внешнего ключа таблицы `supply`
--
ALTER TABLE `supply`
  ADD CONSTRAINT `supply_ibfk_1` FOREIGN KEY (`Part_ID`) REFERENCES `parts` (`Part_ID`),
  ADD CONSTRAINT `supply_ibfk_2` FOREIGN KEY (`Provider_ID`) REFERENCES `providers` (`Provider_ID`),
  ADD CONSTRAINT `supply_ibfk_3` FOREIGN KEY (`Project_ID`) REFERENCES `projects` (`Project_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
