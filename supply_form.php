<form method="get">
    <div class="form-group">
        <label for="part">Деталь</label>
        <select class="custom-select custom-select-md" id="part" name="part">
            <?php
            $query = $pdo->query('SELECT * FROM parts');
            while ($row = $query->fetch())
            {
                echo "<option value=".$row['Part_ID'].">".$row['Part_name']."</option>";
            }?>
        </select>
    </div>
    <div class="form-group">
        <label for="provider">Поставщик</label>
        <select class="custom-select custom-select-md" id="provider" name="provider">
            <?php
            $query = $pdo->query('SELECT * FROM providers');
            while ($row = $query->fetch())
            {
                echo "<option value=".$row['Provider_ID'].">".$row['Provider_name']."</option>";
            }?>
        </select>
    </div>
    <div class="form-group">
        <label for="project">Проект</label>
        <select class="custom-select custom-select-md" id="project" name="project">
            <?php
            $query = $pdo->query('SELECT * FROM projects');
            while ($row = $query->fetch())
            {
                echo "<option value=".$row['Project_ID'].">".$row['Project_name']."</option>";
            }?>
        </select>
    </div>
    <div class="form-group">
        <label for="begin_date">Дата начала</label>
        <input type="datetime-local" class="form-control" id="begin_date" name="begin_date" >
    </div>
    <div class="form-group">
        <label for="end_date">Дата конца</label>
        <input type="datetime-local" class="form-control" id="end_date" name="end_date" >
    </div>
    <div class="form-group">
        <label for="quantity">Количество</label>
        <input type="text" class="form-control" id="quantity" name="quantity">
    </div>
    <div class="form-group">
        <label for="price">Цена</label>
        <input type="text" class="form-control" id="price" name="price">
    </div>
    <input type="hidden" name="C" value="4" >
    <input type="hidden" name="A" value="1" >
    <button type="submit" class="btn btn-primary">Добавить</button>
</form>