<div class="container">
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Деталь</th>
            <th scope="col">Поставщик</th>
            <th scope="col">Проект</th>
            <th scope="col">Дата начала</th>
            <th scope="col">Дата конца</th>
            <th scope="col">Кол-во</th>
            <th scope="col">Цена</th>
            <?php if(isset($_SESSION['username'])) echo "<th scope='col'>Действие</th>" ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $query = $pdo->query('SELECT Supply_ID, parts.Part_name, providers.Provider_name, projects.Project_name, supply.Begin_date, supply.End_date, supply.Quantity, supply.Price FROM parts, providers, projects, supply WHERE parts.Part_ID=supply.Part_ID AND providers.Provider_ID=supply.Provider_ID AND projects.Project_ID=supply.Project_ID');
        while ($row = $query->fetch())
        {
?>

    <tr>
            <th scope='row'><?php echo $row['Supply_ID']?></th>
            <td><?php echo $row['Part_name']?></td>
            <td><?php echo $row['Provider_name']?></td>
            <td><?php echo $row['Project_name']?></td>
            <td><?php echo $row['Begin_date']?></td>
            <td><?php echo $row['End_date']?></td>
            <td><?php echo $row['Quantity']?></td>
            <td><?php echo $row['Price']?></td>
            <?php if(isset($_SESSION['username'])) echo "<td><a type='button' class='btn btn-danger' href='?C=4&A=2&supply_id=".$row['Supply_ID']."'> Удалить</a></td>";?>
        </tr>
<?php
        }
        ?>
        </tbody>
    </table>





</div>
